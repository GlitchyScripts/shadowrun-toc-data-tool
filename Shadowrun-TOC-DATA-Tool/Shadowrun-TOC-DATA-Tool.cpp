#include <windows.h>
#include <stdio.h>
#include <thread>
#include <mutex>
#include <list>
#include "resource.h"
#include "utils/utils.hpp"

// ==== .TOC & .DATA Resource Package File Format ====
// Filesize of .TOC cannot exceed 0x6400000 (limit specified in Shadowrun.exe).
// .DATA package resources cannot be placed over chunk border multiples of address 0x00ff0000.

//STRUCTS .TOC

// The SHA1 is of of the entire .DATA file in 0x50000 chunks (per HashTypeA item) (or the remainder for the last one).
// each chunk must be prepended with this dumb 0x10 byte array salt.
//		HashTypeA (0x20 bytes) [
//			20 bytes: byte array: SHA1
//			12 bytes: byte array: 0 padding
//		]

//		ItemB (0x20 bytes) [
//XXX		32 bytes: byte array: 0 padding (NOT EXPLICITLY VERIFIED)
//		]

#pragma pack(push, 1)
#pragma warning(push)
#pragma warning(disable:4200)
typedef struct {
	uint32_t resourceSize;
	uint32_t resourceOffset;
	// XXX unknown exactly what this does.
	uint64_t resourceClassObjectTypeNode;
	uint16_t resourceFilePathSize;
	// XXX unknown exactly what this does.
	uint8_t resourceFlag;
	// Not null terminated char array string.
	char resourceFilePath[0];
} TOC_PACKAGE_ITEM_RESOURCE;
#pragma warning(pop)
#pragma pack(pop)

//		ItemResource (0x13 + x bytes) [
//			4 bytes: uint32 little-endian: sizeof resource.
//			4 bytes: uint32 little-endian: offset of resource in .DATA file.
//XXX		8 bytes: class, object type, node ???
//			2 bytes: uint16 little-endian: resource filepath size x (not null terminated).
//XXX		1 byte : uint8: some flag 0 to 9 i think.
//			x bytes: char array: resource filepath.
//		]

//		HashTypeD (0x20 bytes) [
//XXX		20 bytes: byte array: SHA1? (NOT EXPLICITLY VERIFIED)
//XXX		12 bytes: byte array: 0 padding. (NOT EXPLICITLY VERIFIED)
//		]

//END STRUCTS

//BEGIN .TOC PACKAGE FILE LAYOUT
//		4 bytes: string: magic "1rrs".
//XXX	4 bytes: uint32 little-endian: version?
//		4 bytes: uint32 little-endian: numOfResources number of file items in .DATA
//XXX	4 bytes: uint32 little-endian: sizeof(Resource * numOfResources) in .DATA. this sizeofResourceArrayInData is not what i thought it was.
//XXX	0x10 bytes: byte array: Hash? (NOT EXPLICITLY VERIFIED)
//		4 bytes: uint32 little-endian: .DATA file size divided by 0x50000 rounded up.
//		4 bytes: uint32 little-endian: sizeof ItemResource array.
//		4 bytes: string: magic "dssb".
//		HashTypeA * numOfData0x50000SizedChunks.
//		ItemB * numOfData0x50000SizedChunks. (NOT EXPLICITLY VERIFIED)
//		ItemResource * NumOfItemsB.
//		0x80 bytes: byte array: Encryption signature 1 for everything up until this point.
//XXX	20 bytes: byte array: SHA1? (NOT EXPLICITLY VERIFIED)
//XXX	12 bytes: byte array: 0 padding. (NOT EXPLICITLY VERIFIED)
//XXX	0x60 bytes: byte array: ZeroPadding1 0 padding. (NOT EXPLICITLY VERIFIED)
//		4 bytes: uint32 little-endian: Number of HashTypeD.
//		HashTypeD * Number of HashTypeD.
//		0x80 bytes: byte array: Encryption signature 2 for everything up until this point (the whole file except last 0x80 bytes).
//EOF

//BEGIN .DATA PACKAGE FILE LAYOUT
//		Resource * numOfResources.
//EOF

// ==== ====

// ==== Some research notes of Shadowrun.exe Patch 3 SHA256: c03c4e4e5b04ed972b013b5a124c8e37b339b5c58a1ae82ff1f2c0df95e6d567 ====
// func_00071C30_ResetDataPosToStart
// func_00084BD0_IsIdentical32ByteArraysAka32BitArraysOf8
// func_00086050_LoadResourcePackage
// func_00087660_GetDataShaHash
// func_00087730_IsDataHashCryptographicallyValid
// func_0008B410_CreateFileW
// func_00097c30_AbortWithPleaseReinstallMessageBox

// i believe this stack of calls is in another thread and is verified after .TOC is completely read.
// 0x000be000 0x000be043 calls v explicitly.
// 0x000bce70 0x000bcf06 calls v. this function might read the target inner file of .DATA by SetFilePointer and ReadFile.
// 0x000bd760 0x000bd7c0 calls v.
// 0x000996d0 0x00099722 calls v explicitly.
// 0x000984e0 this function contains some checking looks like 8 byte hash comparison and if fails aborts with reinstall error func. Calls F004(...).

// 0x00085450 process TOC file entry. func 0x00089150 goes deeper.
// 0x0009d2a8 EAX holds pointer to: 8 bytes: class, object type, node ???

// ==== ====

static bool DoesDataMatchCryptographicSignature(const uint8_t *data, const uint32_t data_size, const uint8_t *signature, const uint32_t signature_size)
{
	HCRYPTPROV hProv;
	bool successAcquire = !!CryptAcquireContextA(&hProv, "Container", 0, PROV_RSA_FULL, 0);
	
	// RSA1.
	const uint8_t keyData[] = { 0x06, 0x02, 0x00, 0x00, 0x00, 0x24, 0x00, 0x00, 0x52, 0x53, 0x41, 0x31, 0x00, 0x04, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x27, 0xD8, 0xFA, 0xE1, 0x17, 0x89, 0xBB, 0xF0, 0xAF, 0x62, 0x07, 0x9E, 0x07, 0x09, 0xAC, 0x5B, 0xDA, 0x15, 0x42, 0x1F, 0x93, 0x83, 0x62, 0xFE, 0x7D, 0x90, 0x0D, 0x81, 0xC8, 0xB8, 0x79, 0xB5, 0x0B, 0x70, 0x83, 0x9C, 0xE2, 0xE3, 0xE4, 0x3A, 0x4B, 0x05, 0xDD, 0x25, 0x1E, 0x3F, 0xB8, 0xA9, 0x93, 0xCC, 0x1B, 0x28, 0xE3, 0x1D, 0xD7, 0xDF, 0x5A, 0xEF, 0x95, 0x9E, 0x9D, 0xFF, 0x76, 0x7E, 0xD7, 0xF6, 0xF8, 0x3A, 0xAB, 0x87, 0x3C, 0xBC, 0xD2, 0xD6, 0x3E, 0xAD, 0xE8, 0x5A, 0x55, 0x6C, 0x0B, 0x5F, 0xFF, 0xA6, 0x8D, 0x30, 0x80, 0x6B, 0x56, 0x61, 0xE9, 0x71, 0x45, 0x8A, 0xFA, 0xEE, 0x7F, 0x68, 0xA3, 0x72, 0x98, 0xF6, 0x1A, 0x82, 0x64, 0x9C, 0x96, 0xA2, 0x88, 0x04, 0x88, 0x9D, 0x5F, 0xB2, 0x77, 0x9A, 0x82, 0x3F, 0x28, 0xBF, 0xBA, 0x41, 0x49, 0x54, 0x45, 0x25, 0xBA, 0xB5 };
	const uint32_t keyDataSize = sizeof(keyData);
	if (keyDataSize != 0x94) {
		__debugbreak();
	}
	
	HCRYPTKEY phKey;
	bool successImportKey = !!CryptImportKey(hProv, keyData, keyDataSize, 0, 0, &phKey);
	
	HCRYPTHASH phHash;
	bool successCreate = !!CryptCreateHash(hProv, CALG_SHA, 0, 0, &phHash);
	
	bool successHash = !!CryptHashData(phHash, data, data_size, 0);
	
	uint8_t hashSha[0x14];//20
	uint32_t hashShaSize = sizeof(hashSha);
	bool successGetHash = !!CryptGetHashParam(phHash, HP_HASHVAL, hashSha, (DWORD*)&hashShaSize, 0);
	if (hashShaSize != sizeof(hashSha)) {
		__debugbreak();
	}
	
	const char szDescription[] = { 0, 0, 0, 0 };
	
	bool successVerifySignature = !!CryptVerifySignatureA(phHash, signature, signature_size, phKey, szDescription, 0);
	
	bool successDestroyHash = !!CryptDestroyHash(phHash);
	
	bool successDestroyKey = !!CryptDestroyKey(phKey);
	
	bool successRelease = !!CryptReleaseContext(hProv, 0);
	
	return successVerifySignature;
}

static bool GetSha1OfSaltPlusData(const uint8_t *data, const uint32_t data_size, uint8_t *hashSha)
{
	HCRYPTPROV hProv;
	bool successAcquire = !!CryptAcquireContextA(&hProv, "Container", 0, PROV_RSA_FULL, 0);
	
	HCRYPTHASH phHash;
	bool successCreate = !!CryptCreateHash(hProv, CALG_SHA, 0, 0, &phHash);
	
	const uint8_t hashSalt[] = { 0x99, 0x09, 0x6C, 0xAB, 0x85, 0x90, 0x44, 0x4A, 0xA2, 0x80, 0xD2, 0xF2, 0x6F, 0x75, 0x99, 0x16 };
	bool successHash = !!CryptHashData(phHash, hashSalt, sizeof(hashSalt), 0);
	
	successHash = !!CryptHashData(phHash, data, data_size, 0);
	
	uint32_t hashShaSize = 0x14;
	bool successGetHash = !!CryptGetHashParam(phHash, HP_HASHVAL, hashSha, (DWORD*)&hashShaSize, 0);
	if (hashShaSize != 0x14) {
		__debugbreak();
	}
	
	bool successDestroyHash = !!CryptDestroyHash(phHash);
	
	bool successRelease = !!CryptReleaseContext(hProv, 0);
	
	return successGetHash;
}

static bool ReadWholeFile(const wchar_t *filepath, uint8_t **fileBuffer, uint32_t *fileBufferSize)
{
	FILE *fileHandle = 0;
	errno_t errorFopen = _wfopen_s(&fileHandle, filepath, L"rb");
	if (!fileHandle) {
		if (errorFopen == ENOENT) {
			printf("%s error file not found: \"%ls\".\n"
				, __func__
				, filepath
			);
		}
		else {
			printf("%s file read error: %d, \"%ls\".\n"
				, __func__
				, errorFopen
				, filepath
			);
		}
		return false;
	}
	
	fseek(fileHandle, (long)0, SEEK_END);
	size_t fileSize = ftell(fileHandle);
	fseek(fileHandle, (long)0, SEEK_SET);
	fileSize -= ftell(fileHandle);
	uint8_t *buffer = new uint8_t[fileSize];
	size_t readC = fread(buffer, sizeof(uint8_t), fileSize / sizeof(uint8_t), fileHandle);
	
	fclose(fileHandle);
	fileHandle = 0;
	
	if (readC != fileSize) {
		delete[] buffer;
		buffer = 0;
		
		printf("%s error did not read whole file (whole 0x%08x) (read 0x%08x) (file \"%ls\").\n"
			, __func__
			, fileSize
			, readC
			, filepath
		);
		return false;
	}
	
	*fileBuffer = buffer;
	*fileBufferSize = readC;
	
	return true;
}

static FILE* OpenWriteFile(const wchar_t *filepath)
{
	FILE *fileHandle = 0;
	errno_t errorFopen = _wfopen_s(&fileHandle, filepath, L"w+b");
	if (!fileHandle) {
		if (errorFopen == EACCES || errorFopen == EIO || errorFopen == EPERM) {
			printf("%s error failed to open file for writing due to insufficient permissions: \"%ls\".\n"
				, __func__
				, filepath
			);
		}
		else if (errorFopen == ESRCH) {
			printf("%s error failed to open file for writing due to path not existing: \"%ls\".\n"
				, __func__
				, filepath
			);
		}
		else {
			printf("%s file write error: %d, \"%ls\".\n"
				, __func__
				, errorFopen
				, filepath
			);
		}
	}
	
	return fileHandle;
}

static bool WriteBufferToFile(const wchar_t *filepath, const uint8_t *fileBuffer, const uint32_t fileBufferSize)
{
	FILE *fileHandle = OpenWriteFile(filepath);
	if (!fileHandle) {
		return false;
	}
	
	uint32_t writtenSize = fwrite(fileBuffer, sizeof(uint8_t), fileBufferSize, fileHandle);
	
	fclose(fileHandle);
	fileHandle = 0;
	
	if (writtenSize != fileBufferSize) {
		printf("%s error did not write whole file (whole 0x%08x) (written 0x%08x) (file \"%ls\").\n"
			, __func__
			, fileBufferSize
			, writtenSize
			, filepath
		);
		return false;
	}
	
	return true;
}

static void RemoveSpecialDirectoryNames(char *path, uint32_t path_size)
{
	uint32_t remainingCharsToCheck = path_size;
	uint32_t nextDirectoryNameSize = 0;
	do {
		char *nextDirectoryNameToCheck = LastPathItem(path, remainingCharsToCheck);
		nextDirectoryNameSize = remainingCharsToCheck;
		remainingCharsToCheck = nextDirectoryNameToCheck - path;
		nextDirectoryNameSize -= remainingCharsToCheck;
		
		if (
			memcmp("./", nextDirectoryNameToCheck, nextDirectoryNameSize) == 0
			|| memcmp("../", nextDirectoryNameToCheck, nextDirectoryNameSize) == 0
			|| memcmp(".\\", nextDirectoryNameToCheck, nextDirectoryNameSize) == 0
			|| memcmp("..\\", nextDirectoryNameToCheck, nextDirectoryNameSize) == 0
		) {
			CustomMemCpy(nextDirectoryNameToCheck, &nextDirectoryNameToCheck[nextDirectoryNameSize], &path[path_size] - &nextDirectoryNameToCheck[nextDirectoryNameSize], true);
		}
	} while (remainingCharsToCheck > 0);
}

static bool VerifyResourcePackage(
	const uint8_t *file_buffer_toc
	, const uint32_t file_buffer_toc_size
	, const uint8_t *file_buffer_data
	, const uint32_t file_buffer_data_size
	, bool verifyEncryption
)
{
	bool resultPerfect = true;
	if (!file_buffer_toc || !file_buffer_toc_size) {
		printf("%s error cannot verify TOC resource package if not given or empty.\n"
			, __func__
		);
		return false;
	}
	
	if (file_buffer_toc_size < 4) {
		printf("%s error TOC resource package smaller than 4 cannot verify magic header.\n"
			, __func__
		);
		return false;
	}
	
	if (memcmp(file_buffer_toc, "1rrs", 4) != 0) {
		printf("%s error TOC resource package magic header does not match expected \"1rrs\".\n"
			, __func__
		);
		return false;
	}
	
	if (file_buffer_toc_size < 8) {
		printf("%s error TOC resource package smaller than 8 cannot verify version.\n"
			, __func__
		);
		return false;
	}
	
	// XXX I think this value represents version.
	if (*(uint32_t*)&file_buffer_toc[4] != 1) {
		printf("%s error TOC resource package version does not match expected value of 1.\n"
			, __func__
		);
		return false;
	}
	
	uint32_t &version = *(uint32_t*)&file_buffer_toc[4];
	
	if (file_buffer_toc_size > 0x6400000) {
		printf("%s error TOC resource package file larger than maximum (0x6400000) specified by game code.\n"
			, __func__
		);
		resultPerfect = false;
	}
	
	uint32_t minimumTocFileSize = (
		// "1rrs" header magic.
		4
		// XXX version?
		+ 4
		// numOfResources.
		+ 4
		// sizeof(Resource * numOfResources) in .DATA
		+ 4
		// XXX Hash?
		+ 0x10
		// numOfData0x50000SizedChunks.
		+ 4
		// sizeof ItemResource array.
		+ 4
		// "dssb" magic.
		+ 4
		// Encryption signature 1.
		+ 0x80
		// XXX SHA1?
		+ 20
		// XXX 0 padding.
		+ 12
		// XXX 0 padding.
		+ 0x60
		// Number of HashTypeD.
		+ 4
		// Encryption signature 2.
		+ 0x80
	);
	if (file_buffer_toc_size < minimumTocFileSize) {
		printf("%s error TOC resource package file smaller than expected (0x%08x). Cannot parse header.\n"
			, __func__
			, minimumTocFileSize
		);
		return false;
	}
	
	uint32_t &numOfResources = *(uint32_t*)&file_buffer_toc[8];
	uint32_t &sizeofResourceArrayInData = *(uint32_t*)&file_buffer_toc[0xc];
	uint8_t *hash1Header = (uint8_t*)&file_buffer_toc[0x10];
	uint32_t &numOfData0x50000SizedChunks = *(uint32_t*)&file_buffer_toc[0x20];
	uint32_t &sizeofItemResourceArray = *(uint32_t*)&file_buffer_toc[0x24];
	
	uint32_t calculatedNumOfData0x50000SizedChunks = (file_buffer_data_size / 0x50000) + ((file_buffer_data_size % 0x50000) ? 1 : 0);
	if (file_buffer_data && numOfData0x50000SizedChunks != calculatedNumOfData0x50000SizedChunks) {
		printf("%s error TOC resource package numOfData0x50000SizedChunks (0x%x) not expected value (0x%x).\n"
			, __func__
			, numOfData0x50000SizedChunks
			, calculatedNumOfData0x50000SizedChunks
		);
		resultPerfect = false;
	}
	
	if (file_buffer_data && sizeofResourceArrayInData > file_buffer_data_size) {
		printf("%s error TOC resource package sizeofResourceArrayInData (0x%x) is greater than size of .DATA resource package (0x%x).\n"
			, __func__
			, sizeofResourceArrayInData
			, file_buffer_data_size
		);
		resultPerfect = false;
	}
	
	if (memcmp(&file_buffer_toc[0x28], "dssb", 4) != 0) {
		printf("%s error TOC resource package offset magic value does not match expected \"dssb\".\n"
			, __func__
		);
		return false;
	}
	
	minimumTocFileSize = (
		// Header size.
		0x2c
		// HashTypeA * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// ItemB * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// sizeof ItemResource array.
		+ sizeofItemResourceArray
	);
	if (file_buffer_toc_size < minimumTocFileSize) {
		printf("%s error TOC resource package file smaller than expected (0x%08x). Cannot parse HashTypeA, ItemB and ItemResource array.\n"
			, __func__
			, minimumTocFileSize
		);
		return false;
	}
	
	uint8_t *hashTypeAArray = (uint8_t*)&file_buffer_toc[0x2c];
	for (uint32_t iHashTypeA = 0; iHashTypeA < numOfData0x50000SizedChunks; iHashTypeA++) {
		uint8_t *hashTypeA = &hashTypeAArray[0x20 * iHashTypeA];
		
		// Verify SHA1 Hash.
		if (file_buffer_data) {
			const uint8_t *bufferLocation = &file_buffer_data[0x50000 * iHashTypeA];
			uint32_t bufferSize = 0x50000;
			// If it is the last remainder chunk.
			if (iHashTypeA + 1 == numOfData0x50000SizedChunks && (file_buffer_data_size % 0x50000)) {
				bufferSize = (file_buffer_data_size % 0x50000);
			}
			if ((uint32_t)bufferLocation + bufferSize > (uint32_t)file_buffer_data + file_buffer_data_size) {
				printf("%s error TOC resource package HashTypeA index 0x%x SHA1 plus salt cannot be verified as the .DATA resource package is not big enough.\n"
					, __func__
					, iHashTypeA
				);
				resultPerfect = false;
			}
			else {
				uint8_t hashOfChunk[20];
				if (!GetSha1OfSaltPlusData(bufferLocation, bufferSize, hashOfChunk)) {
					printf("%s error TOC resource package HashTypeA index 0x%x SHA1 plus salt could not be calculated.\n"
						, __func__
						, iHashTypeA
					);
					resultPerfect = false;
				}
				else if (memcmp(hashTypeA, hashOfChunk, 20) != 0) {
					printf("%s error TOC resource package HashTypeA index 0x%x SHA1 plus salt is incorrect.\n"
						, __func__
						, iHashTypeA
					);
					resultPerfect = false;
				}
			}
		}
		
		// Verify 0 padding.
		for (uint32_t iPosPadding = 0; iPosPadding < 12; iPosPadding++) {
			if (hashTypeA[20 + iPosPadding] != 0x00) {
				printf("%s error TOC resource package HashTypeA index 0x%x does not have expected 0 padding.\n"
					, __func__
					, iHashTypeA
				);
				resultPerfect = false;
				break;
			}
		}
	}
	
	uint8_t *itemBArray = (uint8_t*)&file_buffer_toc[0x2c + (0x20 * numOfData0x50000SizedChunks)];
	for (uint32_t iItemB = 0; iItemB < numOfData0x50000SizedChunks; iItemB++) {
		uint8_t *itemB = &itemBArray[0x20 * iItemB];
		// Verify 0 padding.
		for (uint32_t iPosItemB = 0; iPosItemB < 0x20; iPosItemB++) {
			if (itemB[iPosItemB] != 0x00) {
				printf("%s error TOC resource package ItemB index 0x%x is not all zeros.\n"
					, __func__
					, iItemB
				);
				resultPerfect = false;
				break;
			}
		}
	}
	
	uint8_t *itemResourceArray = (uint8_t*)&file_buffer_toc[0x2c + (0x20 * numOfData0x50000SizedChunks) + (0x20 * numOfData0x50000SizedChunks)];
	{
		uint8_t *itemResourceArrayEnd = (uint8_t*)&itemResourceArray[sizeofItemResourceArray];
		uint8_t *iPosItemResource = itemResourceArray;
		
		uint32_t iPosDataFile = 0;
		
		for (uint32_t iItemResource = 0; iItemResource < numOfResources; iItemResource++) {
			TOC_PACKAGE_ITEM_RESOURCE *itemResource = (TOC_PACKAGE_ITEM_RESOURCE*)iPosItemResource;
			
			if (
				(iPosItemResource + sizeof(TOC_PACKAGE_ITEM_RESOURCE)) > itemResourceArrayEnd
				|| (iPosItemResource += sizeof(TOC_PACKAGE_ITEM_RESOURCE) + itemResource->resourceFilePathSize) > itemResourceArrayEnd
			) {
				printf("%s error TOC resource package file could not fit all ItemResource elements in designated space and stopped at 0x%x of 0x%x.\n"
					, __func__
					, iItemResource + 1
					, numOfResources
				);
				resultPerfect = false;
				break;
			}
			
			if (itemResource->resourceFlag > 0x9) {
				printf("%s error TOC resource package file invalid ItemResource Flag on element 0x%x.\n"
					, __func__
					, iItemResource + 1
				);
				resultPerfect = false;
			}
			
			bool ignorePaddingData = false;
			
			uint32_t chunkPre = (iPosDataFile) / 0x00ff0000;
			uint32_t chunkPost = (iPosDataFile + itemResource->resourceSize) / 0x00ff0000;
			if (chunkPre != chunkPost) {
				ignorePaddingData = true;
				uint32_t paddingSize = ((chunkPre + 1) * 0x00ff0000) - iPosDataFile;
				iPosDataFile += paddingSize;
			}
			
			if (iPosDataFile != itemResource->resourceOffset) {
				printf("%s error TOC resource package file invalid ItemResource offset in .DATA file on element 0x%x.\n"
					, __func__
					, iItemResource + 1
				);
				iPosDataFile = itemResource->resourceOffset;
				resultPerfect = false;
			}
			
			iPosDataFile += itemResource->resourceSize;
			
			// TODO sizeofResourceArrayInData is not what i thought it was.
			//if (iPosDataFile > sizeofResourceArrayInData) {
			//	printf("%s error TOC resource package file invalid ItemResource size in .DATA file extends beyond sizeofResourceArrayInData (0x%08x) on element 0x%x.\n"
			//		, __func__
			//		, sizeofResourceArrayInData
			//		, iItemResource + 1
			//	);
			//	resultPerfect = false;
			//}
			
			{
				uint8_t paddingSize = iPosDataFile % 0x10;
				if (paddingSize) {
					paddingSize = 0x10 - paddingSize;
				
					if (file_buffer_data) {
						if (iPosDataFile + paddingSize > file_buffer_data_size) {
							printf("%s error DATA resource package file not big enough as it does not have space for 0x10 alignment padding on end of resource 0x%x.\n"
								, __func__
								, iItemResource + 1
							);
							resultPerfect = false;
						}
						else {
							// The padding is weird.
							//if (!ignorePaddingData && iPosDataFile < 0x00ff0000) {
							//	for (uint8_t iPadding = 0; iPadding < paddingSize; iPadding++) {
							//		uint32_t iPosPadding = iPosDataFile + iPadding;
							//		if (
							//			(iPosPadding % 2)
							//			? file_buffer_data[iPosPadding] != 0xFF
							//			: file_buffer_data[iPosPadding] != 0xB1
							//		) {
							//			printf("%s error DATA resource package file invalid padding on end of resource 0x%x (does not match 0xB1, 0xFF repeating pattern).\n"
							//				, __func__
							//				, iItemResource + 1
							//			);
							//			resultPerfect = false;
							//			break;
							//		}
							//	}
							//}
						}
					}
				}
			
				iPosDataFile += paddingSize;
			}
		}
		
		// The official files always seem to have random data/files.
		//if (file_buffer_data && file_buffer_data_size > iPosDataFile) {
		//	printf("%s error DATA resource package file has extra data at end of file.\n"
		//		, __func__
		//	);
		//	resultPerfect = false;
		//}
	}
	
	minimumTocFileSize = (
		// Header size.
		0x2c
		// HashTypeA * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// ItemB * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// sizeof ItemResource array.
		+ sizeofItemResourceArray
		// Encryption signature 1.
		+ 0x80
	);
	if (file_buffer_toc_size < minimumTocFileSize) {
		printf("%s error TOC resource package file smaller than expected (0x%08x). Cannot verify EncryptionSignature1.\n"
			, __func__
			, minimumTocFileSize
		);
		return false;
	}
	
	uint8_t *encryptionSignature1 = (uint8_t*)&file_buffer_toc[0x2c + (0x20 * numOfData0x50000SizedChunks) + (0x20 * numOfData0x50000SizedChunks) + sizeofItemResourceArray];
	if (verifyEncryption && !DoesDataMatchCryptographicSignature(file_buffer_toc, (uint32_t)encryptionSignature1 - (uint32_t)file_buffer_toc, encryptionSignature1, 0x80)) {
		printf("%s error TOC resource package EncryptionSignature1 is invalid.\n"
			, __func__
		);
		resultPerfect = false;
	}
	
	minimumTocFileSize = (
		// Header size.
		0x2c
		// HashTypeA * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// ItemB * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// sizeof ItemResource array.
		+ sizeofItemResourceArray
		// Encryption signature 1.
		+ 0x80
		// XXX SHA1?
		+ 20
		// XXX 0 padding.
		+ 12
	);
	if (file_buffer_toc_size < minimumTocFileSize) {
		printf("%s error TOC resource package file smaller than expected (0x%08x). Cannot verify Hash2.\n"
			, __func__
			, minimumTocFileSize
		);
		return false;
	}
	
	uint8_t *hash2 = (uint8_t*)&file_buffer_toc[0x2c + (0x20 * numOfData0x50000SizedChunks) + (0x20 * numOfData0x50000SizedChunks) + sizeofItemResourceArray + 0x80];
	// TODO verify hash.
	
	// Verify Hash2 0 padding.
	for (uint32_t iPosHash2Padding = 0; iPosHash2Padding < 12; iPosHash2Padding++) {
		if (hash2[20 + iPosHash2Padding] != 0x00) {
			printf("%s error TOC resource package Hash2 does not have expected 0 padding.\n"
				, __func__
			);
			resultPerfect = false;
			break;
		}
	}
	
	minimumTocFileSize = (
		// Header size.
		0x2c
		// HashTypeA * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// ItemB * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// sizeof ItemResource array.
		+ sizeofItemResourceArray
		// Encryption signature 1.
		+ 0x80
		// XXX SHA1?
		+ 20
		// XXX 0 padding.
		+ 12
		// XXX 0 padding.
		+ 0x60
	);
	if (file_buffer_toc_size < minimumTocFileSize) {
		printf("%s error TOC resource package file smaller than expected (0x%08x). Cannot verify ZeroPadding1.\n"
			, __func__
			, minimumTocFileSize
		);
		return false;
	}
	
	uint8_t *zeroPadding1 = (uint8_t*)&file_buffer_toc[0x2c + (0x20 * numOfData0x50000SizedChunks) + (0x20 * numOfData0x50000SizedChunks) + sizeofItemResourceArray + 0x80 + 0x20];
	
	// Verify ZeroPadding1.
	for (uint32_t iPosItemB = 0; iPosItemB < 0x20; iPosItemB++) {
		if (zeroPadding1[iPosItemB] != 0x00) {
			printf("%s error TOC resource package ZeroPadding1 is not all zeros.\n"
				, __func__
			);
			resultPerfect = false;
			break;
		}
	}
	
	minimumTocFileSize = (
		// Header size.
		0x2c
		// HashTypeA * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// ItemB * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// sizeof ItemResource array.
		+ sizeofItemResourceArray
		// Encryption signature 1.
		+ 0x80
		// XXX SHA1?
		+ 20
		// XXX 0 padding.
		+ 12
		// XXX 0 padding.
		+ 0x60
		// Number of HashTypeD.
		+ 4
	);
	if (file_buffer_toc_size < minimumTocFileSize) {
		printf("%s error TOC resource package file smaller than expected (0x%08x). Cannot parse NumofHashTypeD.\n"
			, __func__
			, minimumTocFileSize
		);
		return false;
	}
	
	uint32_t &numOfHashTypeD = *(uint32_t*)&file_buffer_toc[0x2c + (0x20 * numOfData0x50000SizedChunks) + (0x20 * numOfData0x50000SizedChunks) + sizeofItemResourceArray + 0x80 + 0x20 + 0x60];
	
	minimumTocFileSize = (
		// Header size.
		0x2c
		// HashTypeA * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// ItemB * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// sizeof ItemResource array.
		+ sizeofItemResourceArray
		// Encryption signature 1.
		+ 0x80
		// XXX SHA1?
		+ 20
		// XXX 0 padding.
		+ 12
		// XXX 0 padding.
		+ 0x60
		// Number of HashTypeD.
		+ 4
		// HashTypeD * numOfHashTypeD.
		+ (0x20 * numOfHashTypeD)
	);
	if (file_buffer_toc_size < minimumTocFileSize) {
		printf("%s error TOC resource package file smaller than expected (0x%08x). Cannot parse HashTypeA, ItemB and ItemResource array.\n"
			, __func__
			, minimumTocFileSize
		);
		return false;
	}
	
	uint8_t *hashTypeDArray = (uint8_t*)&file_buffer_toc[0x2c + (0x20 * numOfData0x50000SizedChunks) + (0x20 * numOfData0x50000SizedChunks) + sizeofItemResourceArray + 0x80 + 0x20 + 0x60 + 4];
	for (uint32_t iHashTypeD = 0; iHashTypeD < numOfHashTypeD; iHashTypeD++) {
		uint8_t *hashTypeD = &hashTypeDArray[0x20 * iHashTypeD];
		
		// Verify SHA1 Hash.
		// TODO
		
		// Verify 0 padding.
		for (uint32_t iPosPadding = 0; iPosPadding < 12; iPosPadding++) {
			if (hashTypeD[20 + iPosPadding] != 0x00) {
				printf("%s error TOC resource package HashTypeD index 0x%x does not have expected 0 padding.\n"
					, __func__
					, iHashTypeD
				);
				resultPerfect = false;
				break;
			}
		}
	}
	
	minimumTocFileSize = (
		// Header size.
		0x2c
		// HashTypeA * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// ItemB * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// sizeof ItemResource array.
		+ sizeofItemResourceArray
		// Encryption signature 1.
		+ 0x80
		// XXX SHA1?
		+ 20
		// XXX 0 padding.
		+ 12
		// XXX 0 padding.
		+ 0x60
		// Number of HashTypeD.
		+ 4
		// HashTypeD * numOfHashTypeD.
		+ (0x20 * numOfHashTypeD)
		// Encryption signature 2.
		+ 0x80
	);
	if (file_buffer_toc_size < minimumTocFileSize) {
		printf("%s error TOC resource package file smaller than expected (0x%08x). Cannot verify EncryptionSignature2.\n"
			, __func__
			, minimumTocFileSize
		);
		return false;
	}
	
	uint8_t *encryptionSignature2 = (uint8_t*)&file_buffer_toc[0x2c + (0x20 * numOfData0x50000SizedChunks) + (0x20 * numOfData0x50000SizedChunks) + sizeofItemResourceArray + 0x80 + 0x20 + 0x60 + 4 + (0x20 * numOfHashTypeD)];
	if (verifyEncryption && !DoesDataMatchCryptographicSignature(file_buffer_toc, (uint32_t)encryptionSignature2 - (uint32_t)file_buffer_toc, encryptionSignature2, 0x80)) {
		printf("%s error TOC resource package EncryptionSignature2 is invalid.\n"
			, __func__
		);
		resultPerfect = false;
	}
	
	if (file_buffer_toc_size > minimumTocFileSize) {
		printf("%s error TOC resource package file larger than expected (0x%08x).\n"
			, __func__
			, minimumTocFileSize
		);
		resultPerfect = false;
	}
	
	return resultPerfect;
}

static bool ExtractResourcePackage(
	const uint8_t *file_buffer_toc
	, const uint32_t file_buffer_toc_size
	, const uint8_t *file_buffer_data
	, const uint32_t file_buffer_data_size
	, const wchar_t *resource_destination_path
	, const wchar_t *resource_package_filename
)
{
	if (!file_buffer_toc || !file_buffer_toc_size || !file_buffer_data || !file_buffer_data_size || !resource_destination_path) {
		printf("%s error one of the arguments to the function are NULL.\n"
			, __func__
		);
		return false;
	}
	
	uint32_t minimumTocFileSize = (
		// Header size.
		0x2c
		// Encryption signature 1.
		+ 0x80
		// XXX SHA1?
		+ 20
		// XXX 0 padding.
		+ 12
		// XXX 0 padding.
		+ 0x60
		// Number of HashTypeD.
		+ 4
		// Encryption signature 2.
		+ 0x80
	);
	if (file_buffer_toc_size < minimumTocFileSize) {
		printf("%s error TOC resource package file is smaller than expected so cannot extract anything.\n"
			, __func__
		);
		return false;
	}
	
	if (memcmp(file_buffer_toc, "1rrs", 4) != 0) {
		printf("%s error TOC resource package magic header does not match expected \"1rrs\". Attempting to extract anyway.\n"
			, __func__
		);
	}
	
	uint32_t &version = *(uint32_t*)&file_buffer_toc[4];
	uint32_t &numOfResources = *(uint32_t*)&file_buffer_toc[8];
	uint32_t &sizeofResourceArrayInData = *(uint32_t*)&file_buffer_toc[0xc];
	uint8_t *hash1Header = (uint8_t*)&file_buffer_toc[0x10];
	uint32_t &numOfData0x50000SizedChunks = *(uint32_t*)&file_buffer_toc[0x20];
	uint32_t &sizeofItemResourceArray = *(uint32_t*)&file_buffer_toc[0x24];
	
	if (memcmp(&file_buffer_toc[0x28], "dssb", 4) != 0) {
		printf("%s error TOC resource package offset magic value does not match expected \"dssb\". Attempting to extract anyway.\n"
			, __func__
		);
	}
	
	minimumTocFileSize = (
		// Header size.
		0x2c
		// HashTypeA * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// ItemB * numOfData0x50000SizedChunks.
		+ (0x20 * numOfData0x50000SizedChunks)
		// sizeof ItemResource array.
		+ sizeofItemResourceArray
	);
	if (file_buffer_toc_size < minimumTocFileSize) {
		printf("%s error TOC resource package file smaller than expected (0x%08x). Cannot parse HashTypeA, ItemB and ItemResource array.\n"
			, __func__
			, minimumTocFileSize
		);
		return false;
	}
	
	wchar_t *resourcePackageDestinationFilepath = FormMallocString(L"%s%s.toc.data.csv", resource_destination_path, resource_package_filename);
	
	FILE *resourcePackageFile = 0;
	uint32_t errorEnsureDirectoryExistsResourceList = EnsureDirectoryExists(resource_destination_path);
	if (errorEnsureDirectoryExistsResourceList != ERROR_SUCCESS) {
		printf("%s error resource filepath could not be created with error 0x%08x \"%ls\".\n"
			, __func__
			, errorEnsureDirectoryExistsResourceList
			, resource_destination_path
		);
	}
	else {
		resourcePackageFile = OpenWriteFile(resourcePackageDestinationFilepath);
	}
	
	delete[] resourcePackageDestinationFilepath;
	resourcePackageDestinationFilepath = 0;
	
	if (!resourcePackageFile) {
		return false;
	}
	
	bool resultSuccess = true;
	
	uint8_t *itemResourceArray = (uint8_t*)&file_buffer_toc[0x2c + (0x20 * numOfData0x50000SizedChunks) + (0x20 * numOfData0x50000SizedChunks)];
	{
		uint8_t *itemResourceArrayEnd = (uint8_t*)&itemResourceArray[sizeofItemResourceArray];
		uint8_t *iPosItemResource = itemResourceArray;
		
		for (uint32_t iItemResource = 0; iItemResource < numOfResources; iItemResource++) {
			TOC_PACKAGE_ITEM_RESOURCE *itemResource = (TOC_PACKAGE_ITEM_RESOURCE*)iPosItemResource;
			
			if (
				(iPosItemResource + sizeof(TOC_PACKAGE_ITEM_RESOURCE)) > itemResourceArrayEnd
				|| (iPosItemResource += sizeof(TOC_PACKAGE_ITEM_RESOURCE) + itemResource->resourceFilePathSize) > itemResourceArrayEnd
			) {
				printf("%s error TOC resource package file could not fit all ItemResource elements in designated space and stopped at 0x%x of 0x%x.\n"
					, __func__
					, iItemResource + 1
					, numOfResources
				);
				resultSuccess = false;
				break;
			}
			
			if (
				itemResource->resourceOffset > file_buffer_data_size
				|| itemResource->resourceOffset + itemResource->resourceSize < itemResource->resourceOffset
				|| itemResource->resourceOffset + itemResource->resourceSize > file_buffer_data_size
			) {
				printf("%s error TOC resource package file element 0x%x cannot be extracted as the resource offset and size are invalid.\n"
					, __func__
					, iItemResource + 1
				);
				resultSuccess = false;
				continue;
			}
			
			uint32_t sanitisedResourceFilePathSize = itemResource->resourceFilePathSize + 1;
			char *sanitisedResourceFilePath = new char[sanitisedResourceFilePathSize];
			
			memcpy(sanitisedResourceFilePath, itemResource->resourceFilePath, itemResource->resourceFilePathSize);
			sanitisedResourceFilePath[itemResource->resourceFilePathSize] = 0;
			
			RemoveSpecialDirectoryNames(sanitisedResourceFilePath, sanitisedResourceFilePathSize);
			
			char *sanitisedResourceFilePathForIO = CloneString(sanitisedResourceFilePath);
			ReplaceFilePathSensitiveChars(sanitisedResourceFilePathForIO, false);
			
			wchar_t *resourceDestinationFilepath = FormMallocString(L"%s%s/%hs", resource_destination_path, resource_package_filename, sanitisedResourceFilePathForIO);
			
			delete[] sanitisedResourceFilePathForIO;
			sanitisedResourceFilePathForIO = 0;
			
			uint32_t resourceDestinationFilepathExtendedPathFullSize = GetFullPathNameW(resourceDestinationFilepath, 0, 0, 0) + 1;
			wchar_t *resourceDestinationFilepathExtendedPathFull = new wchar_t[resourceDestinationFilepathExtendedPathFullSize];
			resourceDestinationFilepathExtendedPathFull[0] = 0;
			uint32_t resourceDestinationFilepathExtendedPathFullSize2 = GetFullPathNameW(resourceDestinationFilepath, resourceDestinationFilepathExtendedPathFullSize, resourceDestinationFilepathExtendedPathFull, 0) + 1;
			
			wchar_t *resourceDestinationFilepathExtendedPath = FormMallocString(L"\\\\?\\%s", resourceDestinationFilepathExtendedPathFull);
			
			delete[] resourceDestinationFilepathExtendedPathFull;
			resourceDestinationFilepathExtendedPathFull = 0;
			
			wchar_t *resourceDestinationPath = PathFromFilename(resourceDestinationFilepath);
			uint32_t errorEnsureDirectoryExistsResource = EnsureDirectoryExists(resourceDestinationPath);
			if (errorEnsureDirectoryExistsResource != ERROR_SUCCESS) {
				printf("%s error TOC resource package file element 0x%x cannot be extracted as the directory the resource is to be placed at could not be created with error 0x%08x \"%ls\".\n"
					, __func__
					, iItemResource + 1
					, errorEnsureDirectoryExistsResource
					, resourceDestinationPath
				);
				resultSuccess = false;
			}
			else {
				bool successWriteResource = WriteBufferToFile(resourceDestinationFilepathExtendedPath, &file_buffer_data[itemResource->resourceOffset], itemResource->resourceSize);
				if (successWriteResource) {
					char *itemResourceSimple = FormMallocString("0x%016llx,0x%02hhx,%s\n", itemResource->resourceClassObjectTypeNode, itemResource->resourceFlag, sanitisedResourceFilePath);
					
					fputs(itemResourceSimple, resourcePackageFile);
					
					free(itemResourceSimple);
					itemResourceSimple = 0;
				}
			}
			
			delete sanitisedResourceFilePath;
			sanitisedResourceFilePath = 0;
			
			delete[] resourceDestinationPath;
			resourceDestinationPath = 0;
			
			free(resourceDestinationFilepathExtendedPath);
			resourceDestinationFilepathExtendedPath = 0;
			
			free(resourceDestinationFilepath);
			resourceDestinationFilepath = 0;
		}
	}
	
	fclose(resourcePackageFile);
	resourcePackageFile = 0;
	
	return resultSuccess;
}

static bool PackageResources(
	const wchar_t *resource_package_listing_filepath
)
{
	if (!EndsWithCaseInsensitive(resource_package_listing_filepath, L".toc.data.csv")) {
		printf("%s error resource package listing filename must end with \".toc.data.csv\".\n"
			, __func__
		);
		return false;
	}
	
	uint8_t *fileBufferListing = 0;
	uint32_t fileBufferListingSize = 0;
	bool successReadListing = ReadWholeFile(resource_package_listing_filepath, &fileBufferListing, &fileBufferListingSize);
	if (!successReadListing) {
		return false;
	}
	
	wchar_t *resourcesPath = CloneString(resource_package_listing_filepath);
	uint32_t resourcesPathLen = wcslen(resourcesPath);
	
	memcpy(&resourcesPath[resourcesPathLen-13], L".toc", 5 * sizeof(wchar_t));
	wchar_t *resourcePackageFilepathToc = CloneString(resourcesPath);
	
	memcpy(&resourcesPath[resourcesPathLen-13], L".data", 6 * sizeof(wchar_t));
	wchar_t *resourcePackageFilepathData = CloneString(resourcesPath);
	
	memset(&resourcesPath[resourcesPathLen-13], 0, 13 * sizeof(wchar_t));
	resourcesPath[resourcesPathLen-13] = L'/';
	resourcesPathLen -= 12;
	
	FILE *resourcePackageFileToc = OpenWriteFile(resourcePackageFilepathToc);
	FILE *resourcePackageFileData = OpenWriteFile(resourcePackageFilepathData);
	
	if (!resourcePackageFileToc || !resourcePackageFileData) {
		if (resourcePackageFileToc) {
			fclose(resourcePackageFileToc);
			resourcePackageFileToc = 0;
		}
		if (resourcePackageFileData) {
			fclose(resourcePackageFileData);
			resourcePackageFileData = 0;
		}
		delete[] resourcesPath;
		resourcesPath = 0;
		delete[] resourcePackageFilepathToc;
		resourcePackageFilepathToc = 0;
		delete[] resourcePackageFilepathData;
		resourcePackageFilepathData = 0;
		
		delete[] fileBufferListing;
		fileBufferListing = 0;
		
		return false;
	}
	
	std::list<TOC_PACKAGE_ITEM_RESOURCE*> itemResources;
	
	uint8_t *iPosListing = fileBufferListing;
	uint32_t iListingLine = 0;
	while (iPosListing < &fileBufferListing[fileBufferListingSize]) {
		uint32_t remainingSize = &fileBufferListing[fileBufferListingSize] - iPosListing;
		
		char *currentLine = (char*)iPosListing;
		iListingLine++;
		{
			char *nextLineR = strchr(currentLine, '\r');
			char *nextLineN = strchr(currentLine, '\n');
			
			char *nextLine = nextLineR;
			if (nextLineN && (!nextLineR || nextLineN < nextLine)) {
				nextLine = nextLineN + sizeof(char);
				*nextLineN = 0;
				if (nextLineR && nextLineR == nextLineN + 1) {
					*nextLineR = 0;
					nextLine += sizeof(char);
				}
			}
			else if (nextLineR) {
				*nextLineR = 0;
				nextLine += sizeof(char);
			}
			
			if (nextLine) {
				iPosListing = (uint8_t*)nextLine;
				remainingSize = nextLine - currentLine;
			}
			else {
				iPosListing = &fileBufferListing[fileBufferListingSize];
			}
		}
		
		char *resourceClassObjectTypeNodeText = currentLine;
		char *resourceFlagText = (char*)memchr(resourceClassObjectTypeNodeText, ',', remainingSize);
		char *resourceFilepath = resourceFlagText ? (char*)memchr(resourceFlagText + 1, ',', remainingSize - (resourceFlagText - currentLine)) : 0;
		if (!resourceFlagText || !resourceFilepath) {
			printf("%s error resource package listing file line %u is not valid as it is missing a comma.\n"
				, __func__
				, iListingLine
			);
			continue;
		}
		
		*resourceFlagText = 0;
		resourceFlagText++;
		*resourceFilepath = 0;
		resourceFilepath++;
		
		uint32_t resourceFilepathLen = strnlen(resourceFilepath, remainingSize - (resourceFilepath - currentLine));
		if (resourceFilepathLen == 0) {
			printf("%s error resource package listing file line %u is not valid as the resource path is empty.\n"
				, __func__
				, iListingLine
			);
			continue;
		}
		
		TOC_PACKAGE_ITEM_RESOURCE *itemResource = (TOC_PACKAGE_ITEM_RESOURCE*)malloc(sizeof(TOC_PACKAGE_ITEM_RESOURCE) + resourceFilepathLen + sizeof(char));
		itemResource->resourceOffset = 0;
		itemResource->resourceSize = 0;
		memcpy(itemResource->resourceFilePath, resourceFilepath, resourceFilepathLen);
		itemResource->resourceFilePath[resourceFilepathLen] = 0;
		itemResource->resourceFilePathSize = resourceFilepathLen;
		
		if (sscanf_s(resourceClassObjectTypeNodeText, "0x%llx", &itemResource->resourceClassObjectTypeNode) != 1) {
			printf("%s error resource package listing file line %u is not valid as the resource ClassObjectTypeNode cannot be parsed as a hexadecimal number.\n"
				, __func__
				, iListingLine
			);
			free(itemResource);
			itemResource = 0;
			continue;
		}
		
		if (sscanf_s(resourceFlagText, "0x%hhx", &itemResource->resourceFlag) != 1) {
			printf("%s error resource package listing file line %u is not valid as the resource Flag cannot be parsed as a hexadecimal number.\n"
				, __func__
				, iListingLine
			);
			free(itemResource);
			itemResource = 0;
			continue;
		}
		
		itemResources.push_back(itemResource);
	}
	
	bool success = true;
	
	delete[] fileBufferListing;
	fileBufferListing = 0;
	
	uint32_t iPosResourcePackageData = 0;
	uint32_t itemResourcesSize = 0;
	
	uint8_t paddingBufferB1FF[0x10];
	for (uint32_t iBuf = 0; iBuf < sizeof(paddingBufferB1FF); iBuf++) {
		if (iBuf % 2) {
			paddingBufferB1FF[iBuf] = 0xFF;
		}
		else {
			paddingBufferB1FF[iBuf] = 0xB1;
		}
	}
	
	for (TOC_PACKAGE_ITEM_RESOURCE *&itemResource : itemResources) {
		itemResourcesSize += sizeof(TOC_PACKAGE_ITEM_RESOURCE) + itemResource->resourceFilePathSize;
		
		wchar_t *itemResourceFilepath = FormMallocString(L"%s%hs", resourcesPath, itemResource->resourceFilePath);
		ReplaceFilePathSensitiveChars(itemResourceFilepath, false);
		
		uint8_t *fileBufferResourceData = 0;
		uint32_t fileBufferResourceDataSize = 0;
		bool successReadResourceData = ReadWholeFile(itemResourceFilepath, &fileBufferResourceData, &fileBufferResourceDataSize);
		if (!successReadResourceData) {
			free(itemResourceFilepath);
			itemResourceFilepath = 0;
			continue;
		}
		
		if (fileBufferResourceDataSize > 0x00ff0000) {
			printf("%s WARNING resource \"%ls\" is larger than 0x00ff0000 in size. Unknown if the game will accept this (without error or crash).\n"
				, __func__
				, itemResourceFilepath
			);
		}
		
		uint32_t chunkPre = (iPosResourcePackageData) / 0x00ff0000;
		uint32_t chunkPost = (iPosResourcePackageData + fileBufferResourceDataSize) / 0x00ff0000;
		if (chunkPre != chunkPost) {
			uint32_t paddingSize = ((chunkPre + 1) * 0x00ff0000) - iPosResourcePackageData;
			if (paddingSize % 0x10) {
				__debugbreak();
			}
			paddingSize /= 0x10;
			while (paddingSize-- > 0) {
				uint32_t writtenPaddingSize = fwrite(paddingBufferB1FF, sizeof(uint8_t), 0x10, resourcePackageFileData);
				iPosResourcePackageData += writtenPaddingSize;
				if (writtenPaddingSize != 0x10) {
					printf("%s error failed to pad space before resource file \"%ls\" in .DATA resource package.\n"
						, __func__
						, itemResourceFilepath
					);
					success = false;
					break;
				}
			}
		}
		
		uint32_t writtenSize = fwrite(fileBufferResourceData, sizeof(uint8_t), fileBufferResourceDataSize, resourcePackageFileData);
		
		delete[] fileBufferResourceData;
		fileBufferResourceData = 0;
		
		if (writtenSize != fileBufferResourceDataSize) {
			printf("%s error did not write whole resource file \"%ls\" to .DATA resource package (whole 0x%08x) (written 0x%08x).\n"
				, __func__
				, itemResourceFilepath
				, fileBufferResourceDataSize
				, writtenSize
			);
			success = false;
		}
		
		free(itemResourceFilepath);
		itemResourceFilepath = 0;
		
		itemResource->resourceOffset = iPosResourcePackageData;
		itemResource->resourceSize = writtenSize;
		iPosResourcePackageData += writtenSize;
		
		uint8_t paddingSize = iPosResourcePackageData % 0x10;
		if (paddingSize) {
			paddingSize = 0x10 - paddingSize;
		}
		fwrite(&paddingBufferB1FF[iPosResourcePackageData % 2], sizeof(uint8_t), paddingSize, resourcePackageFileData);
		iPosResourcePackageData += paddingSize;
	}
	
	{
		uint8_t paddingBufferZero[0x80];
		memset(paddingBufferZero, 0, sizeof(paddingBufferZero));
		
		uint8_t paddingBufferAE[0x80];
		memset(paddingBufferAE, 0xAE, sizeof(paddingBufferAE));
		
		uint8_t paddingBufferEC[0x80];
		memset(paddingBufferEC, 0xEC, sizeof(paddingBufferEC));
		
		uint32_t tempuint32 = 0;
		
		uint32_t iPosResourcePackageToc = 0;
		
		#define WRITE_TO_TOC(buffer, buffer_size) fwrite((buffer), sizeof(uint8_t), (iPosResourcePackageToc += (buffer_size), (buffer_size)), resourcePackageFileToc);
		
		WRITE_TO_TOC("1rrs", 4);
		WRITE_TO_TOC((tempuint32 = 0x00000001, &tempuint32), sizeof(uint32_t));
		WRITE_TO_TOC((tempuint32 = itemResources.size(), &tempuint32), sizeof(uint32_t));
		WRITE_TO_TOC((tempuint32 = iPosResourcePackageData, &tempuint32), sizeof(uint32_t));
		WRITE_TO_TOC(paddingBufferAE, 0x10);
		uint32_t numOfData0x50000SizedChunks = (iPosResourcePackageData / 0x50000) + ((iPosResourcePackageData % 0x50000) ? 1 : 0);
		WRITE_TO_TOC(&numOfData0x50000SizedChunks, sizeof(uint32_t));
		WRITE_TO_TOC(&itemResourcesSize, sizeof(uint32_t));
		WRITE_TO_TOC("dssb", 4);
		
		uint8_t *data0x50000SizedChunk = new uint8_t[0x50000];
		fseek(resourcePackageFileData, 0, SEEK_SET);
		
		for (uint32_t iHashTypeA = 0; iHashTypeA < numOfData0x50000SizedChunks; iHashTypeA++) {
			uint32_t bufferChunkSize = 0x50000;
			// If it is the last remainder chunk.
			if (iHashTypeA + 1 == numOfData0x50000SizedChunks && (iPosResourcePackageData % 0x50000)) {
				bufferChunkSize = (iPosResourcePackageData % 0x50000);
			}
			
			uint32_t readSize = fread(data0x50000SizedChunk, sizeof(uint8_t), bufferChunkSize, resourcePackageFileData);
			uint8_t hashOfChunk[20];
			bool successShaData = false;
			if (readSize != bufferChunkSize) {
				printf("%s error TOC resource package HashTypeA index 0x%x chunk was not read in whole (requested: 0x%08x. actual: 0x%08x).\n"
					, __func__
					, iHashTypeA
					, bufferChunkSize
					, readSize
				);
				success = false;
			}
			else {
				successShaData = GetSha1OfSaltPlusData(data0x50000SizedChunk, readSize, hashOfChunk);
				if (!successShaData) {
					printf("%s error TOC resource package HashTypeA index 0x%x SHA1 plus salt could not be calculated.\n"
						, __func__
						, iHashTypeA
					);
					success = false;
				}
			}
			
			if (!successShaData) {
				WRITE_TO_TOC(paddingBufferAE, 20);
			}
			else {
				WRITE_TO_TOC(hashOfChunk, 20);
			}
			WRITE_TO_TOC(paddingBufferZero, 12);
		}
		
		delete[] data0x50000SizedChunk;
		data0x50000SizedChunk = 0;
		
		for (uint32_t iItemB = 0; iItemB < numOfData0x50000SizedChunks; iItemB++) {
			WRITE_TO_TOC(paddingBufferZero, 0x20);
		}
		
		for (TOC_PACKAGE_ITEM_RESOURCE *&itemResource : itemResources) {
			WRITE_TO_TOC(itemResource, sizeof(TOC_PACKAGE_ITEM_RESOURCE) + itemResource->resourceFilePathSize);
		}
		
		{
			uint8_t *tocEncryptionData = new uint8_t[iPosResourcePackageToc];
			
			fseek(resourcePackageFileToc, 0, SEEK_SET);
			
			uint32_t readSize = fread(tocEncryptionData, sizeof(uint8_t), iPosResourcePackageToc, resourcePackageFileToc);
			
			// Must set file position after fread if intending to follow with an fwrite.
			fseek(resourcePackageFileToc, iPosResourcePackageToc, SEEK_SET);
			
			if (readSize != iPosResourcePackageToc) {
				printf("%s error TOC resource package Encryption signature 1 data was not read in whole (requested: 0x%08x. actual: 0x%08x).\n"
					, __func__
					, iPosResourcePackageToc
					, readSize
				);
				WRITE_TO_TOC(paddingBufferEC, 0x80);
				success = false;
			}
			else {
				// TODO generate encryption signature.
				WRITE_TO_TOC(paddingBufferEC, 0x80);
			}
			
			delete[] tocEncryptionData;
			tocEncryptionData = 0;
		}
		
		WRITE_TO_TOC(paddingBufferAE, 20);
		WRITE_TO_TOC(paddingBufferZero, 12);
		
		WRITE_TO_TOC(paddingBufferZero, 0x60);
		
		// XXX unknown.
		uint32_t numHashTypeD = 0;
		
		WRITE_TO_TOC((tempuint32 = numHashTypeD, &tempuint32), sizeof(uint32_t));
		
		for (uint32_t iHashTypeD = 0; iHashTypeD < numHashTypeD; iHashTypeD++) {
			WRITE_TO_TOC(paddingBufferZero, 0x20);
		}
		
		{
			uint8_t *tocEncryptionData = new uint8_t[iPosResourcePackageToc];
			
			fseek(resourcePackageFileToc, 0, SEEK_SET);
			
			uint32_t readSize = fread(tocEncryptionData, sizeof(uint8_t), iPosResourcePackageToc, resourcePackageFileToc);
			
			// Must set file position after fread if intending to follow with an fwrite.
			fseek(resourcePackageFileToc, iPosResourcePackageToc, SEEK_SET);
			
			if (readSize != iPosResourcePackageToc) {
				printf("%s error TOC resource package Encryption signature 2 data was not read in whole (requested: 0x%08x. actual: 0x%08x).\n"
					, __func__
					, iPosResourcePackageToc
					, readSize
				);
				WRITE_TO_TOC(paddingBufferEC, 0x80);
				success = false;
			}
			else {
				// TODO generate encryption signature.
				WRITE_TO_TOC(paddingBufferEC, 0x80);
			}
			
			delete[] tocEncryptionData;
			tocEncryptionData = 0;
		}
	}
	
	fclose(resourcePackageFileData);
	resourcePackageFileData = 0;
	
	fclose(resourcePackageFileToc);
	resourcePackageFileToc = 0;
	
	for (TOC_PACKAGE_ITEM_RESOURCE *&itemResource : itemResources) {
		free(itemResource);
	}
	itemResources.clear();
	
	delete[] resourcesPath;
	resourcesPath = 0;
	delete[] resourcePackageFilepathToc;
	resourcePackageFilepathToc = 0;
	delete[] resourcePackageFilepathData;
	resourcePackageFilepathData = 0;
	
	return success;
}

namespace EXEC_FLAGS {
	enum Type : uint8_t {
		UNKNOWN = 0,
		VERSION,
		HELP,
		TOC,
		DATA,
		INTEGRITY,
		EXTRACT,
		PACKAGE,
		IGNORE_ENCRYPTION_SIGNATURE,
	};
}

static EXEC_FLAGS::Type WhichFlag(wchar_t *arg)
{
	size_t argFlagLen = wcslen(arg);
	wchar_t *equals = wcschr(arg, L'=');
	if (equals) {
		argFlagLen = (equals - arg);
	}
	
	if (_wcsnicmp(L"-V", arg, argFlagLen) == 0 || _wcsnicmp(L"--version", arg, argFlagLen) == 0) {
		return EXEC_FLAGS::VERSION;
	}
	else if (_wcsnicmp(L"-T", arg, argFlagLen) == 0 || _wcsnicmp(L"--toc", arg, argFlagLen) == 0) {
		return EXEC_FLAGS::TOC;
	}
	else if (_wcsnicmp(L"-D", arg, argFlagLen) == 0 || _wcsnicmp(L"--data", arg, argFlagLen) == 0) {
		return EXEC_FLAGS::DATA;
	}
	else if (_wcsnicmp(L"-I", arg, argFlagLen) == 0 || _wcsnicmp(L"--integrity", arg, argFlagLen) == 0) {
		return EXEC_FLAGS::INTEGRITY;
	}
	else if (_wcsnicmp(L"-E", arg, argFlagLen) == 0 || _wcsnicmp(L"--extract", arg, argFlagLen) == 0) {
		return EXEC_FLAGS::EXTRACT;
	}
	else if (_wcsnicmp(L"-P", arg, argFlagLen) == 0 || _wcsnicmp(L"--package", arg, argFlagLen) == 0) {
		return EXEC_FLAGS::PACKAGE;
	}
	else if (_wcsnicmp(L"-IES", arg, argFlagLen) == 0 || _wcsnicmp(L"--ignore-encryption-signature", arg, argFlagLen) == 0) {
		return EXEC_FLAGS::IGNORE_ENCRYPTION_SIGNATURE;
	}
	else if (_wcsnicmp(L"-H", arg, argFlagLen) == 0
		|| _wcsnicmp(L"/H", arg, argFlagLen) == 0
		|| _wcsnicmp(L"-help", arg, argFlagLen) == 0
		|| _wcsnicmp(L"--help", arg, argFlagLen) == 0
		|| _wcsnicmp(L"/help", arg, argFlagLen) == 0
		|| _wcsnicmp(L"/?", arg, argFlagLen) == 0
		|| _wcsnicmp(L"-?", arg, argFlagLen) == 0
		|| _wcsnicmp(L"--?", arg, argFlagLen) == 0
	) {
		return EXEC_FLAGS::HELP;
	}
	
	return EXEC_FLAGS::UNKNOWN;
}

int main()
{
	int result = EXIT_SUCCESS;
	wchar_t *resourcePackageFilepathToc = 0;
	wchar_t *resourcePackageFilepathData = 0;
	bool performIntegrityCheck = false;
	wchar_t *resourcePackageExtractPath = 0;
	wchar_t *resourcePackageListingFilepath = 0;
	bool ignoreEncryptionSignature = false;
	
	int nArgs;
	// GetCommandLineW() does not need de-allocating but ToArgv does.
	LPWSTR *lpwszArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);
	if (lpwszArglist == NULL) {
		uint32_t errorCmdLineToArgv = GetLastError();
		printf("%s CommandLineToArgvW(...) failed with error 0x%08x."
			, __func__
			, errorCmdLineToArgv
		);
		return 1;
	}
	
	bool abort = false;
	bool printHelp = false;
	bool versionPrinted = false;
	
	#define ExecFlagExit iArgs = nArgs; abort = true; break;
	#define ExecFlagExitFailure iArgs = nArgs; result = EXIT_FAILURE; break;
	#define ExecFlagExitFailurePrintHelp printHelp = true; ExecFlagExitFailure;
	#define RequireExecFlagValue(flagValue) {\
		flagValue = wcschr(lpwszArglist[iArgs], L'=');\
		if (!flagValue) {\
			printf("%ls requires an additional parameter.\n", lpwszArglist[iArgs]);\
			ExecFlagExitFailurePrintHelp;\
		}\
		flagValue++;\
	}
	
	for (int iArgs = 1; iArgs < nArgs; iArgs++) {
		EXEC_FLAGS::Type execFlag = WhichFlag(lpwszArglist[iArgs]);
		switch(execFlag) {
			case EXEC_FLAGS::VERSION: {
				printf("Shadowrun-TOC-DATA-Tool.exe version: %hhu.%hhu.%hhu.%hhu.\n", (uint8_t)BINARY_VERSION_MAJOR, (uint8_t)BINARY_VERSION_MINOR, (uint8_t)BINARY_VERSION_REVISION, (uint8_t)BINARY_VERSION_BUILD);
				versionPrinted = true;
				ExecFlagExit;
			}
			case EXEC_FLAGS::TOC: {
				RequireExecFlagValue(resourcePackageFilepathToc);
				break;
			}
			case EXEC_FLAGS::DATA: {
				RequireExecFlagValue(resourcePackageFilepathData);
				break;
			}
			case EXEC_FLAGS::INTEGRITY: {
				performIntegrityCheck = true;
				break;
			}
			case EXEC_FLAGS::EXTRACT: {
				RequireExecFlagValue(resourcePackageExtractPath);
				break;
			}
			case EXEC_FLAGS::PACKAGE: {
				RequireExecFlagValue(resourcePackageListingFilepath);
				break;
			}
			case EXEC_FLAGS::IGNORE_ENCRYPTION_SIGNATURE: {
				ignoreEncryptionSignature = true;
				break;
			}
			case EXEC_FLAGS::HELP: {
				printHelp = true;
				ExecFlagExit;
			}
			default: {
				printf("Unknown flag: \"%ls\".\n", lpwszArglist[iArgs]);
				ExecFlagExitFailurePrintHelp;
			}
		}
	}
	
	if (resourcePackageExtractPath) {
		uint32_t resourcePackageExtractPathLen = wcslen(resourcePackageExtractPath);
		if (resourcePackageExtractPath[resourcePackageExtractPathLen-1] != L'/' && resourcePackageExtractPath[resourcePackageExtractPathLen-1] != L'\\') {
			printf("Error: --package path must end in a slash ('/' or '\\').");
			result = EXIT_FAILURE;
		}
	}
	
	if (resourcePackageExtractPath && resourcePackageListingFilepath) {
		printf("Error: --extract and --package flags were both specified.");
		result = EXIT_FAILURE;
	}
	else if (performIntegrityCheck && resourcePackageListingFilepath) {
		printf("Error: --integrity and --package flags were both specified.");
		result = EXIT_FAILURE;
	}
	
	if (performIntegrityCheck || resourcePackageExtractPath) {
		if (!resourcePackageFilepathToc) {
			printf("Error: --toc flag was not specified.");
			result = EXIT_FAILURE;
		}
		if (resourcePackageExtractPath && !resourcePackageFilepathData) {
			printf("Error: --data flag was not specified.");
			result = EXIT_FAILURE;
		}
	}
	
	if (result == EXIT_SUCCESS && !abort && !versionPrinted && !performIntegrityCheck && !resourcePackageExtractPath && !resourcePackageListingFilepath) {
		printf("Error: No action was specified.");
		result = EXIT_FAILURE;
	}
	
	if (printHelp) {
		printf("Usage: Shadowrun-TOC-DATA-Tool.exe [OPTION]...\n");
		printf(" -V,    --version               Prints out the version of this tool and exits.\n");
		printf(" -T=<>, --toc=<filepath>        The filepath of the .TOC resource package.\n");
		printf(" -D=<>, --data=<filepath>       The filepath of the .DATA resource package.\n");
		printf(" -I,    --integrity             Validate the integrity of the resource package.\n");
		printf(" -E=<>, --extract=<path>        The path to extract the resource package to.\n");
		printf(" -P=<>, --package=<filepath>    The filepath to place the resource package at.\n");
		printf(" -IES,  --ignore-encryption-signature   Ignore when validating integrity.\n");
	}
	
	if (result != EXIT_SUCCESS || abort) {
		LocalFree(lpwszArglist);
		return result;
	}
	
	if (performIntegrityCheck || resourcePackageExtractPath) {
		uint8_t *fileBufferToc = 0;
		uint32_t fileBufferTocSize = 0;
		bool successReadToc = ReadWholeFile(resourcePackageFilepathToc, &fileBufferToc, &fileBufferTocSize);
		if (!successReadToc) {
			LocalFree(lpwszArglist);
			return EXIT_FAILURE;
		}
		
		uint8_t *fileBufferData = 0;
		uint32_t fileBufferDataSize = 0;
		bool successReadData = resourcePackageFilepathData ? ReadWholeFile(resourcePackageFilepathData, &fileBufferData, &fileBufferDataSize) : false;
		if (resourcePackageFilepathData && !successReadData) {
			delete[] fileBufferToc;
			fileBufferToc = 0;
			LocalFree(lpwszArglist);
			return EXIT_FAILURE;
		}
		
		if (performIntegrityCheck) {
			bool successVerifyResourcePackage = VerifyResourcePackage(fileBufferToc, fileBufferTocSize, fileBufferData, fileBufferDataSize, !ignoreEncryptionSignature);
			if (!successVerifyResourcePackage) {
				delete[] fileBufferToc;
				fileBufferToc = 0;
				if (fileBufferData) {
					delete[] fileBufferData;
					fileBufferData = 0;
				}
				LocalFree(lpwszArglist);
				return EXIT_FAILURE;
			}
		}
		
		if (resourcePackageExtractPath) {
			wchar_t *resourcePackageFilename = CloneString(FilenameFromPathname(resourcePackageFilepathToc));
			if (EndsWithCaseInsensitive(resourcePackageFilename, L".toc")) {
				uint32_t resourcePackageFilenameLen = wcslen(resourcePackageFilename);
				memset(&resourcePackageFilename[resourcePackageFilenameLen-4], 0, 4);
			}
			
			bool successExtractResourcePackage = ExtractResourcePackage(fileBufferToc, fileBufferTocSize, fileBufferData, fileBufferDataSize, resourcePackageExtractPath, resourcePackageFilename);
			
			delete[] resourcePackageFilename;
			resourcePackageFilename = 0;
			
			if (!successExtractResourcePackage) {
				delete[] fileBufferToc;
				fileBufferToc = 0;
				if (fileBufferData) {
					delete[] fileBufferData;
					fileBufferData = 0;
				}
				LocalFree(lpwszArglist);
				return EXIT_FAILURE;
			}
		}
		
		delete[] fileBufferToc;
		fileBufferToc = 0;
		if (fileBufferData) {
			delete[] fileBufferData;
			fileBufferData = 0;
		}
	}
	
	if (resourcePackageListingFilepath) {
		bool successPackageResources = PackageResources(resourcePackageListingFilepath);
		if (!successPackageResources) {
			LocalFree(lpwszArglist);
			return EXIT_FAILURE;
		}
	}
	
	LocalFree(lpwszArglist);
	return EXIT_SUCCESS;
}
