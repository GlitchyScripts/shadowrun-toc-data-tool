# Shadowrun-TOC-DATA-Tool
This tool can be used to verify the integrity of, extract, and package new .TOC and .DATA files for Shadowrun released on PC in 2007.

The .TOC file extension likely means Table Of Contents as it basically acts as one for the .DATA file of the same name. The .DATA file is a concatenation of many individual resource files into one big data blob. Together I call them a resource package. A stored resource has a filename as well as 2 other important but unknown values. Using this tool, an extracted resource package will give you a .TOC.DATA.CSV file of the same name as well as a folder of the same name containing all the resources from inside the resource package. The .TOC.DATA.CSV file is a comma separated value list of all the resources which starts with those 2 unknown values then the filename. Filenames that contain reserved IO characters will have those characters swapped with an underscore when stored on disk.

The tool when used will print out any errors if there are any. No errors printed out should always mean success. When the tool exits, an exit code of 0 indicate success and anything else means there was an error (the error itself should also have already been printed out).

Here are some tool usage examples:

Validate integrity of `core.toc` and `core.data` resource package pair:<br>
`Shadowrun-TOC-DATA-Tool.exe "--toc=./core.toc" "--data=./core.data" --integrity`<br>
Use the `--ignore-encryption-signature` execution flag to ignore the encryption signature 1 & 2 errors when validating the integrity of custom made resource packages.

Extract the `core.toc` and `core.data` resource package pair:<br>
`Shadowrun-TOC-DATA-Tool.exe "--toc=./core.toc" "--data=./core.data" "--extract=./extracted/base/"`

Package a new resource package pair and output the result at `./extracted/base/core.toc` and `./extracted/base/core.data`:<br>
`Shadowrun-TOC-DATA-Tool.exe "--package=./extracted/base/core.toc.data.csv"`
